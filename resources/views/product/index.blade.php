@extends('layouts.app')
@section('title', $viewData["title"])
@section('subtitle', $viewData["subtitle"])
@section('content')
<div class="container w-100 ">
  <form action="{{route('products.filter')}}" method="post">
    @csrf
    <div class="d-flex justify-content-end w-100 mb-3">
      <div class="w-25 me-3">
        <select name="categorie" class="form-select">
          @foreach($viewData['categories'] as $categorie)
              <option value="{{$categorie->getId()}}">{{$categorie->getName()}}</option>
          @endforeach
        </select>
      </div>
      <div class="">
          <button class="btn btn-primary">filter</button>
      </div>
    </div>
  </form>
  <div class="row">
    @foreach ($viewData["products"] as $product)
    <div class="col-md-4 col-lg-3 mb-2">
      <div class="card">
        <img src="{{ asset('/storage/'.$product->getImage()) }}" class="card-img-top img-card">
        <div class="card-body text-center">
          <a href="{{ route('product.show', ['id'=> $product->getId()]) }}"
            class="btn bg-primary text-white">{{ $product->getName() }}</a>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
