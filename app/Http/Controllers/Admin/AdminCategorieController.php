<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Categorie;
use Illuminate\Http\Request;

class AdminCategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Categorie - Online Store";
        $viewData["categories"] = Categorie::all();
        return view('admin.categorie.index')->with("viewData", $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $categorie=new Categorie();
        $categorie->setName($request->input('name'));
        $categorie->setDescription($request->input('description'));
        $categorie->save();
        return redirect()->route('admin.categorie.index');
    }
    public function edit(int $id)
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Edit Categorie - Online Store";
        $viewData["categorie"] = Categorie::findOrFail($id);
        return view('admin.categorie.edit')->with("viewData", $viewData);
    }
    public function update(Request $request, int $id)
    {
        //
        $categorie=Categorie::findOrFail($id);
        $categorie->setName($request->input('name'));
        $categorie->setDescription($request->input('description'));
        $categorie->save();
        return redirect()->route('admin.categorie.index');
    }
    public function delete(int $id)
    {
        //
        Categorie::destroy($id);
        return back();
    }
}
