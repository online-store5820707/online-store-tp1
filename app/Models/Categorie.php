<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;
    protected $fillable=['name',"description"];

    public function getId(){
        return $this->attributes['id'];
    }
    public function getName(){
        return $this->attributes['name'];
    }
    public function setName($newName){
        $this->attributes['name']=$newName;
    }
    public function getDescription(){
        return $this->attributes['description'];
    }
    public function setDescription($newDescription){
        $this->attributes['description']=$newDescription;
    }
}
